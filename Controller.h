#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <GL/freeglut.h>
#include "AppController.h"
#include "Rectangle.h"
#include "Button.h"
#include <iostream>

class Controller : public AppController {
    Rectangle** r;
    int count;

    Button button3;
    Button button4;
    Button button5;

    void init() {
        r = new Rectangle*[count];

        for (int i = 0; i < count; i++) {
            r[i] = new Rectangle[count];
        }

        float y = 0.9f;
        float w = 1.8f / count;
        float h = 1.5f / count;

        for (int i = 0; i < count; i++) {
            float x = -0.9f;
            for (int j = 0; j < count; j++) {
                r[i][j] = Rectangle(x, y, w, h);
                x += w;
            }
            y -= h;
        }
    }

public:

    Controller(){
        button3 = Button("3 x 3", -0.9f, -0.75f);
        button4 = Button("4 x 4", -0.3f, -0.75f);
        button5 = Button("5 x 5", 0.3f, -0.75f);

        count = 3;
        init();
    }

    void leftMouseDown(float x, float y) {
        if (button3.contains(x, y)) {
            std::cout << "Create 3 x 3" << std::endl;
            for (int i = 0; i < count; i++) {
                delete[] r[i];
            }
            delete[] r;
            count = 3;
            init();
        }
        else if (button4.contains(x, y)) {
            std::cout << "Create 4 x 4" << std::endl;
            for (int i = 0; i < count; i++) {
                delete[] r[i];
            }
            delete[] r;
            count = 4;
            init();
        }
        else if (button5.contains(x, y)) {
            std::cout << "Create 5 x 5" << std::endl;
            for (int i = 0; i < count; i++) {
                delete[] r[i];
            }
            delete[] r;
            count = 5;
            init();
        }
    };

    void render(){
        button3.draw();
        button4.draw();
        button5.draw();

        for (int i = 0; i < count; i++) {
            for (int j = 0; j < count; j++) {
                r[i][j].draw();
            }
        }
    }
};

#endif